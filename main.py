from tkinter import *
from functions.creer_grille import create_grid
from functions.feu_au_nord import feu_au_nord 
from functions.feu_au_sud import feu_au_sud
from functions.feu_a_ouest import feu_a_ouest
from functions.feu_a_est import feu_a_est
import datetime
import argparse
import random
master = Tk() 

# les différents arguments qui sont à passer #
parser = argparse.ArgumentParser()
parser.add_argument('-rows', const=10, nargs='?', type=int, default=10)
parser.add_argument('-cols', const=20, nargs='?', type=int, default=20)
parser.add_argument('-cell_size', const=35, nargs='?', type=int, default=35)
parser.add_argument('-afforestation', const=0.6, nargs='?', type=float, default=0.6)
args = parser.parse_args()
# ------------------------------------------- #

# création du canvas qui soutient tout le projet #
canvas = Canvas(master, width = args.cell_size*args.cols+300, height = args.cell_size*args.rows+50)
canvas.pack()
# ---------------------------------------------- #

# création de la grille à l'appel de la fonction create_grid #
grid = create_grid(args.rows, args.cols, args.cell_size, args.afforestation, canvas)
# ---------------------------------------------------------- #

# définition de l'animation afin de mettre en feu une cellule cliquée #
def put_fire(event):
    x1 = event.x
    y1 = event.y
    x2 = x1
    y2 = y1
    if event.num == 1:
        for elem in canvas.find_closest(x1,y1):
            canvas.itemconfig(elem, fill="red")
            coord = canvas.coords(elem)
            print(coord)
            #y1 = y1 - args.cell_size # nord
            #y1 = y1 + args.cell_size # sud
            #x1 = x1 + args.cell_size # est
            #x1 = x1 - args.cell_size # west
            feu_au_sud(canvas, args.cell_size, x1, x2, y1, y2)
# ------------------------------------------------------------------- #

canvas.bind('<Button-1>', put_fire)
master.mainloop()