from tkinter import *
import random

def create_grid(rows, cols, cell_size, afforestation, canvas):
    grid = [[None] * cols for _ in range(rows)]
    for i in range(0, cols):
        for j in range(0, rows):
            nbrRandom = random.uniform(0,1)
            x1 = i * cell_size
            y1 = j * cell_size
            x2 = x1 + cell_size
            y2 = y1 + cell_size
            if(nbrRandom < afforestation):
                grid[j][i] = canvas.create_rectangle(x1,y1,x2,y2, fill = 'green', tags="vert")
            else:
                grid[j][i] = canvas.create_rectangle(x1,y1,x2,y2, fill = 'white', tags="blanc")
    return grid